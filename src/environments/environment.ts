// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAk-rxu6u045yJJEWVocFUb0B2U6BS5iME',
    authDomain: 'onlineshopwb-a68c0.firebaseapp.com',
    databaseURL: 'https://onlineshopwb-a68c0.firebaseio.com',
    projectId: 'onlineshopwb-a68c0',
    storageBucket: 'onlineshopwb-a68c0.appspot.com',
    messagingSenderId: '973846847804'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
