export class Product {
  id: number;
  name: string;
  type: number;
  description: string;
  quantity: number;
  price: number;
  shortageTime: Date;
  imageUrl: string;


  constructor(name: string, type: number, description: string, quantity: number, price: number, shortageTime: Date, imageUrl: string) {
    this.name = name;
    this.type = type;
    this.description = description;
    this.quantity = quantity;
    this.price = price;
    this.shortageTime = shortageTime;
    this.imageUrl = imageUrl;
  }

  getTypeString(): string {
    if (this.type === 1) {
      return 'Clothes';
    }
    if (this.type === 2) {
      return 'Sport';
    }
    if (this.type === 3) {
      return 'Chess';
    }
    return '';
  }
}
