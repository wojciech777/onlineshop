import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {ProductsService} from '../services/products-service/products.service';
import {Product} from '../product';

@Component({
  selector: 'app-product-creator',
  templateUrl: './product-creator.component.html',
  styleUrls: ['./product-creator.component.css']
})
export class ProductCreatorComponent implements OnInit {
  @Input() productTemplate: Product;
  currentProductTypes = [{id: 1, name: 'Clothes'},
    {id: 2, name: 'Sport'},
    {id: 3, name: 'Chess'}];

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
  }

  addCreatedProduct(): void {
    console.log('dodajemy nowy produkt o name: ', this.productTemplate.name,
      ' price: ', this.productTemplate.price, ' quantity: ', this.productTemplate.quantity,
      ' description: ', this.productTemplate.description);
    if (!this.productTemplate.name || !this.productTemplate.price || !this.productTemplate.type ||
      (!this.productTemplate.quantity && this.productTemplate.quantity !== 0)
      || !this.productTemplate.description || !this.productTemplate.imageUrl) {
      alert('All fields are required');
    } else {
      if (!this.productTemplate.id && !(this.productTemplate.id === 0)) {
        this.productsService.addNewProduct(this.productTemplate.name, this.productTemplate.type, this.productTemplate.description,
          this.productTemplate.quantity, this.productTemplate.price, this.productTemplate.imageUrl);
        document.getElementById('#modalCloseButton' + this.productTemplate.id).click();
        alert('You have already added new product');
      } else {
        this.productsService.editProduct(this.productTemplate.id, this.productTemplate.name,
          this.productTemplate.type, this.productTemplate.description,
          this.productTemplate.quantity, this.productTemplate.price, this.productTemplate.imageUrl);
        document.getElementById('#modalCloseButton' + this.productTemplate.id).click();
        alert('You have already edited product');
      }
      // this._element.modal("hide");
    }
  }

  private clearFormFields() {
    this.productTemplate.name = '';
    this.productTemplate.type = 2;
    this.productTemplate.description = '';
    this.productTemplate.quantity = undefined;
    this.productTemplate.price = undefined;
    this.productTemplate.imageUrl = '';
  }
}
