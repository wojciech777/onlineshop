import {Product} from './product';

export class ProductOrdered extends Product {
  packed: boolean;

  constructor(product: Product) {
    super(product.name, product.type, product.description, product.quantity, product.price, product.shortageTime, product.imageUrl);
    this.id = product.id;
    this.packed = false;
    // brudny hack - przetwarzanie w chmurze
    if (this.shortageTime === undefined) {
      this.shortageTime = new Date();
    }
  }

  public getSumPrice(): number {
    return this.quantity * this.price;
  }
}
