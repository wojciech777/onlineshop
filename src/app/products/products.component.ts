import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Product} from '../product';
import {PRODUCTS} from '../mock-products';
import {ProductsService} from '../services/products-service/products.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  @Input() productsFiltered: Product[];

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {}

  public orderProduct(productId: number): void {
    console.log('zostal zamowiony produkt o id: ', productId);
    this.productsService.putProductToCart(productId);
  }
}
