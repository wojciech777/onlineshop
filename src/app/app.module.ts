import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import { OrderedProductComponent } from './ordered-product/ordered-product.component';
import { ProductCreatorComponent } from './product-creator/product-creator.component';
import { ProductsSearchingComponent } from './products-searching/products-searching.component';
import { CartSummarizationComponent } from './cart-summarization/cart-summarization.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { OrdersPanelComponent } from './orders-panel/orders-panel.component';
import {environment} from '../environments/environment';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { LoggingComponent } from './logging/logging.component';
import {AuthGuard} from './auth.guard';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AdminProductComponent } from './admin-product/admin-product.component';
import { ProductModalComponent } from './product-modal/product-modal.component';
import { ProbnyComponent } from './probny/probny.component';

const appRoutes: Routes = [
  { path: '',
    redirectTo: '/products',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoggingComponent
  },
  {
    path: 'admin',
    component: AdminPanelComponent,
    canActivate: [AuthGuard],
    children: []
  },
  {
    path: 'products',
    component: ProductsSearchingComponent,
    canActivate: [AuthGuard],
    children: []
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [AuthGuard],
    children: []
  }
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductComponent,
    CartComponent,
    OrderedProductComponent,
    ProductCreatorComponent,
    ProductsSearchingComponent,
    CartSummarizationComponent,
    AdminPanelComponent,
    OrdersPanelComponent,
    LoggingComponent,
    AdminProductComponent,
    ProductModalComponent,
    ProbnyComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    FormsModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
