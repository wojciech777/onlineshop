import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Input() product: Product;
  @Output() orderProduct =  new EventEmitter<number>();

  constructor() {}

  ngOnInit() {}

  public makeOrderProduct(): void {
    this.orderProduct.emit(this.product.id);
  }
}
