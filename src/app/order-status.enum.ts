export enum OrderStatus {
  Pending = 1,
  PartiallyComplete,
  Realized,
}
