import {Product} from './product';
import {OrderStatus} from './order-status.enum';
import {ProductOrdered} from './product-ordered';

export class Order {
  id: number;
  clientId: number;
  destinationAddress: String;
  addressee: String;
  createDate: Date;
  realizationDate: Date;
  estimatedRealizationTime: Date;
  status: OrderStatus;
  products: ProductOrdered[];

  constructor(id: number, clientId: number, destinationAddress: String, addressee: String, products: ProductOrdered[]) {
    this.id = id;
    this.clientId = clientId;
    this.status = OrderStatus.Pending;
    this.destinationAddress = destinationAddress;
    this.addressee = addressee;
    this.products = products;
    this.realizationDate = new Date();
    this.createDate = new Date();
  }

  public getOrderStatusString(): string {
    if (OrderStatus.Realized === this.status) {
      return 'Realized';
    }
    if (OrderStatus.PartiallyComplete === this.status) {
      return 'Partially Complete';
    }
    if (OrderStatus.Pending === this.status) {
      return 'Pending';
    }
    return '';
  }

  public getProductsSumPrice(): number {
    return this.products.reduce(function (previousValue, currentValue) {
      return previousValue + currentValue.getSumPrice();
    }, 0);
  }
}
