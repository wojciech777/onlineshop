import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../product';
import {ProductsService} from '../services/products-service/products.service';

@Component({
  selector: 'app-ordered-product',
  templateUrl: './ordered-product.component.html',
  styleUrls: ['./ordered-product.component.css']
})
export class OrderedProductComponent implements OnInit {
  @Input() product: Product;

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
  }

  public removeWholeOrder(): void {
    console.log('zarzadano usunięcia z koszyka wszysktich elementów od id: ', this.product.id);
    this.productsService.removeAllProductOccurencesFromCart(this.product.id);
  }

  public decreaseOrderQuantity(): void {
    console.log('zarzadano zmniejszenia krotności elementu od id: ', this.product.id);
    this.productsService.removeProductFromCart(this.product.id);
  }
}
