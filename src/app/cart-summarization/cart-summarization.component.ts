import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductsService} from '../services/products-service/products.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-cart-summarization',
  templateUrl: './cart-summarization.component.html',
  styleUrls: ['./cart-summarization.component.css']
})
export class CartSummarizationComponent implements OnInit, OnDestroy {
  private productServiceSubscription: Subscription;
  public overallCost: number;
  public productsNumber: number;

  constructor(private productsService: ProductsService) {}

  ngOnInit() {
    this.productServiceSubscription = this.productsService.getProductsInCart().subscribe(products => {
      this.productsNumber = products.reduce(function (previousValue, currentValue) {
        return previousValue + currentValue.quantity;
      }, 0);
      this.overallCost = products.reduce(function (previousValue, currentValue) {
        return previousValue + currentValue.quantity * currentValue.price;
      }, 0);
    });
  }

  ngOnDestroy(): void {
    this.productServiceSubscription.unsubscribe();
  }
}
