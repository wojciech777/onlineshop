import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductsService} from '../services/products-service/products.service';
import {Product} from '../product';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-products-searching',
  templateUrl: './products-searching.component.html',
  styleUrls: ['./products-searching.component.css']
})
export class ProductsSearchingComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  filteredProducts: Product[] = [];
  private productsFilteredWithoutTypeFilters: Product[];
  productServiceSubscription: Subscription;
  currentProductTypes = [{id: 1, name: 'Clothes'},
    {id: 2, name: 'Sport'},
    {id: 3, name: 'Chess'}];
  selectedProductTypes: number[] = [];

  currentPage = 1;
  pageSize = 10;

    pagesNumber: number;
  nameFilter: string;
  bottomPrice = 0;
  topPrice = 1000;
  sortingByPrice = true;
  sortingByName = false;
  sortingDescending = false;

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.productServiceSubscription = this.productsService.getProducts().subscribe(products => {
      this.products = products.filter(product => product.quantity);
      this.makeEvaluationsAfterFilterChange();
      console.log('searching component dostał dane');
    });
  }

  ngOnDestroy() {
    this.productServiceSubscription.unsubscribe();
  }

  public changeSelectionStatusOfType(typeId: number) {
    const index = this.selectedProductTypes.indexOf(typeId);
    if (index !== -1) {
      this.selectedProductTypes.splice(index, 1);
    } else {
      this.selectedProductTypes.push(typeId);
    }
    this.makeEvaluationsAfterFilterChange();
  }

  public makeEvaluationsAfterFilterChange() {
    this.filteredProducts = this.products;
    this.filterByName();
    this.filterByTopPrice();
    this.filterByBottomPrice();
    this.productsFilteredWithoutTypeFilters = this.filteredProducts;
    this.filterUnselectedProduct();
    this.filteredProducts.sort(this.dynamicSort(this.getSortingColumn()));
    this.evaluatePagesNumber();
    this.navigateToFirstPage();
  }

  getProductTypeQuantity(typeId: number): number {
    return this.productsFilteredWithoutTypeFilters.filter(product => product.type === typeId).length;
  }

  evaluatePagesNumber() {
    this.pagesNumber = Math.ceil(this.filteredProducts.length / this.pageSize);
    console.log('number of pages evaluated');
  }

  getCurrentPageProducts(): Product[] {
    return this.filteredProducts.slice((this.currentPage - 1) * this.pageSize, this.currentPage * this.pageSize);
  }

  getAllPagesNumbers(): number[] {
    return Array.from(new Array(this.pagesNumber), (val, index) => index + 1);
  }

  decreaseCurrentPageNumber() {
    if (this.currentPage > 1) {
      --this.currentPage;
    }
  }

  increaseCurrentPageNumber() {
    if (this.currentPage < this.pagesNumber) {
      ++this.currentPage;
    }
  }

  navigateToFirstPage() {
    this.currentPage = 1;
  }

  changeSortingByName() {
    this.sortingByName = !this.sortingByName;
  }

  changeSortingByPrice() {
    this.sortingByPrice = !this.sortingByPrice;
  }

  private filterUnselectedProduct() {
    this.filteredProducts = this.filteredProducts.filter(this.removeUnselectedProducts, this);
  }

  private removeUnselectedProducts(product: Product) {
    if (!this.selectedProductTypes.length) {
      return true;
    }
    if (this.selectedProductTypes.includes(product.type)) {
      return true;
    }
    return false;
  }

  private filterByName() {
    this.filteredProducts = this.filteredProducts.filter(product => {
      if (!this.nameFilter) {
        return true;
      } else {
        return (product.name.toLowerCase().indexOf(this.nameFilter) !== -1);
      }
    }, this);
  }

  private filterByTopPrice() {
    if ((this.topPrice !== 0) && (!this.topPrice)) {
      return;
    }
    this.filteredProducts = this.filteredProducts.filter(product => product.price <= this.topPrice);
  }

  private filterByBottomPrice() {
    if ((this.bottomPrice !== 0) && (!this.bottomPrice)) {
      return;
    }
    this.filteredProducts = this.filteredProducts.filter(product => product.price >= this.bottomPrice);
  }

  changeSortingDirection() {
    this.sortingDescending = !this.sortingDescending;
  }

  dynamicSort(property) {
    let sortOrder = 1;
    if (property[0] === '-') {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a, b) {
      const result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    };
  }

  getSortingColumn(): String {
    let sortingColumn = '';
    if (this.sortingByName) {
      sortingColumn = 'name';
    } else {
      sortingColumn = 'price';
    }
    if (this.sortingDescending) {
      sortingColumn = '-' + sortingColumn;
    }

    return sortingColumn;
  }
}
