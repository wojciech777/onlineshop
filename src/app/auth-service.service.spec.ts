import { TestBed } from '@angular/core/testing';

import { AuthServiceService } from './auth-service.service';
import {environment} from '../environments/environment';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireModule} from 'angularfire2';

describe('AuthServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireAuthModule,
      AngularFireDatabaseModule
    ],
    schemas: [NO_ERRORS_SCHEMA]}));

  it('should be created', () => {
    const service: AuthServiceService = TestBed.get(AuthServiceService);
    expect(service).toBeTruthy();
  });
});
