import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductsService} from '../services/products-service/products.service';
import {Product} from '../product';
import {Subscription} from 'rxjs';
import {OrdersService} from '../services/orders-service/orders.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {
  private productServiceSubscription: Subscription;
  public productsInCart: Product[] = [];
  private overallCost: number;
  private productsNumber: number;
  public addressee = {value: '', valid: true};
  public address = {value: '', valid: true};

  constructor(private productsService: ProductsService, private ordersService: OrdersService) {}

  ngOnInit() {
    this.productServiceSubscription = this.productsService.getProductsInCart().subscribe(productsInCart => {
        this.productsInCart = productsInCart.filter(product => product.quantity);
        this.productsNumber = this.productsInCart.reduce(function (previousValue, currentValue) {
          return previousValue + currentValue.quantity;
        }, 0);
        this.overallCost = this.productsInCart.reduce(function (previousValue, currentValue) {
          return previousValue + currentValue.quantity * currentValue.price;
        }, 0);
        console.log('cart dostał nowe dane');
      }
    );
  }

  public checkOrderFormValidity(): void {
    if (this.address.value && this.addressee.value) {
      document.getElementById('#modalCloseButton').click();
      this.ordersService.addOrder(this.productsInCart, this.address.value, this.addressee.value);
      this.productsService.markWholeCartAsOrdered();
    }
    if (!this.address.value) {
      this.address.valid = false;
    } else {
      this.address.valid = true;
    }
    if (!this.addressee.value) {
      this.addressee.valid = false;
    } else {
      this.addressee.valid = true;
    }
  }

  ngOnDestroy() {
    this.productServiceSubscription.unsubscribe();
  }
}
