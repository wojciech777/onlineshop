import {Component, OnInit} from '@angular/core';
import {AuthServiceService} from '../auth-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logging',
  templateUrl: './logging.component.html',
  styleUrls: ['./logging.component.css']
})
export class LoggingComponent implements OnInit {
  email: string;
  password: string;

  constructor(public authService: AuthServiceService, private router: Router) {
  }

  ngOnInit() {
  }

  public loginToFirebase() {
    const router = this.router;
    this.authService.login({email: this.email, password: this.password}).then(function () {
      console.log('Logged in to FireBase!');
      router.navigate(['/products']);
    }, function (error) {
      console.log(error.code);
      console.log(error.message);
    });
  }

  /*public registerAtFirebase() {
    this.authService.register({email: this.email, password: this.password}).then(function () {
      console.log('Logged in to FireBase!');
    }, function (error) {
      console.log(error.code);
      console.log(error.message);
    });
  }*/
}
