import {Injectable} from '@angular/core';
import {PRODUCTS} from '../../mock-products';
import {Product} from '../../product';
import {BehaviorSubject, Observable, of, pipe, Subject} from 'rxjs';
import {PRODUCTS_IN_CART} from '../../mock-products-in-cart';
import {forEach} from '@angular/router/src/utils/collection';
import {first, max} from 'rxjs/operators';
import {AngularFireDatabase} from 'angularfire2/database';
import {FirebaseDatabaseService} from '../database-services/firebase-db-service/firebase-database.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private products: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
  private productsInCart: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);

  // private productsInCart: Product[] = [];

  constructor(private firebaseDbManager: FirebaseDatabaseService) {
    this.firebaseDbManager.getProducts().subscribe(value => {
      this.products.next(this.productsSnapshotToProductsArray(value));
    });
    this.firebaseDbManager.getProductsInCart().subscribe(value => {
      this.productsInCart.next(this.productsSnapshotToProductsArray(value));
    });
    console.log('inicjalizacja serwisu produktów');
  }

  getProducts(): Observable<Product[]> {
    return this.products.asObservable();
  }

  getProductsInCart(): Observable<Product[]> {
    return this.productsInCart.asObservable();
  }

  getProduct(productId: number): Product {
    const filteredProducts = this.products.getValue().filter(product => product.id === productId);

    if (filteredProducts.length) {
      return filteredProducts[0];
    }
    return null;
  }


  editProduct(id: number, name: string, type: number, description: string, quantity: number, price: number, imageUrl: string): void {
    this.getProduct(id).name = name;
    this.getProduct(id).type = type;
    this.getProduct(id).description = description;
    this.getProduct(id).quantity = quantity;
    this.getProduct(id).price = price;
    this.getProduct(id).imageUrl = imageUrl;

    const newProductProperties = {
      type: type, name: name,
      description: description, price: price, quantity: quantity, imageUrl: imageUrl
    };

    this.firebaseDbManager.updateProduct(id, newProductProperties);
    this.products.next(this.products.getValue());
  }

  addNewProduct(name: string, type: number, description: string, quantity: number, price: number, imageUrl: string): void {
    const newProductId = this.findMinimalNotUsedProductId();
    const newProduct = {
      id: newProductId, type: type, name: name,
      description: description, price: price, quantity: quantity, imageUrl: imageUrl
    };
    const prod: Product = new Product(name, type, description, quantity, price, new Date(), imageUrl);
    prod.id = newProductId;
    this.products.getValue().push(prod);

    this.firebaseDbManager.updateProduct(newProductId, newProduct);
    this.products.next(this.products.getValue());
    console.log('zostal dodany produkt: ', name, ' o id ', newProductId);
    console.log('obecny stan produktow to: ', this.products.getValue());
    console.log('obecny stan produktow w koszyku to: ', this.productsInCart.getValue());
  }

  removeProduct(productId: number) {
    for (let i = 0; i < this.products.getValue().length; i++) {
      if (this.products.getValue()[i].id === productId) {
        this.products.getValue().splice(i, 1);
      }
    }
    this.firebaseDbManager.removeProduct(productId);
    this.products.next(this.products.getValue());
  }

  removeAllProductOccurencesFromCart(productId: number): void {
    this.productsInCart.getValue().forEach(product => {
      if (product.id === productId) {
        this.increaseProductQuantity(Object.assign({}, product), product.quantity);
        product.quantity = 0;
      }
    });
    this.firebaseDbManager.removeAllProductOccurrencesFromCart(productId);
    this.products.next(this.products.getValue());
    this.productsInCart.next(this.productsInCart.getValue());
  }

  putProductToCart(productId: number): void {
    for (let i = 0; i < this.products.getValue().length; i++) {
      if ((this.products.getValue()[i].id === productId) && (this.products.getValue()[i].quantity > 0)) {
        this.products.getValue()[i].quantity = this.products.getValue()[i].quantity - 1;
        this.increaseProductQuantityInCart(this.products.getValue()[i]);
      }
    }

    this.firebaseDbManager.putProductToCart(productId);
    this.products.next(this.products.getValue());
    this.productsInCart.next(this.productsInCart.getValue());
  }

  removeProductFromCart(productId: number): void {
    this.productsInCart.getValue().forEach(productInCart => {
      if ((productInCart.id === productId) && (productInCart.quantity)) {
        productInCart.quantity = productInCart.quantity - 1;
        this.increaseProductQuantity(productInCart);
      }
    });
    this.firebaseDbManager.removeProductFromCart(productId);
    this.products.next(this.products.getValue());
    this.productsInCart.next(this.productsInCart.getValue());
  }

  markWholeCartAsOrdered() {
    console.log('whole cart is ordered');
    this.productsInCart.getValue().forEach(productInCart => this.firebaseDbManager.updateProductInCart(productInCart.id, {quantity: 0}));
    this.productsInCart.next([]);
  }

  private increaseProductQuantity(newProduct: Product, addedProductsNumber = 1) {
    let quantityIncreased = false;

    this.products.getValue().map(function (product) {
      if (product.id === newProduct.id) {
        quantityIncreased = true;
        product.quantity = product.quantity + addedProductsNumber;
      }
    });

    if (!quantityIncreased) {
      this.products.getValue().push(newProduct);
    }
  }

  private increaseProductQuantityInCart(product: Product) {
    const filteredProducts = this.productsInCart.getValue().filter(value => value.id === product.id);
    if (filteredProducts.length) {
      filteredProducts[0].quantity = filteredProducts[0].quantity + 1;
    } else {
      const productToInsert = Object.assign({}, product);
      productToInsert.quantity = 1;
      this.productsInCart.getValue().push(productToInsert);
    }
    console.log('current available products:', this.products.getValue());
    console.log('current products in cart:', this.productsInCart.getValue());
  }

  private findMinimalNotUsedProductId(): number {
    // póki co zakładam że wszystkie produkty istnieją w bazie produktów nawet jak już nie mają krotności
    let maxFoundIndex = -1;
    this.products.getValue().forEach(product => {
      if (product.id > maxFoundIndex) {
        maxFoundIndex = product.id;
      }
    });
    return maxFoundIndex + 1;
  }

  private productsSnapshotToProductsArray(snapshot): Product[] {
    const returnArr: Product[] = [];

    snapshot.forEach(function (childSnapshot) {
      const item = childSnapshot.payload.val();
      const product: Product = new Product
        (item.name, item.type, item.description, item.quantity, item.price, item.shortageTime, item.imageUrl);
      product.id = Number(childSnapshot.key);

      returnArr.push(product);
    });

    return returnArr;
  }
}
