import {async, inject, TestBed} from '@angular/core/testing';

import {ProductsService} from './products.service';
import {environment} from '../../../environments/environment';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireModule} from 'angularfire2';
import {first, map, skip, take} from 'rxjs/operators';
import {Product} from '../../product';
import {promise} from 'selenium-webdriver';
import filter = promise.filter;

describe('ProductsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireAuthModule,
      AngularFireDatabaseModule
    ]
  }));

  it('service should be created', inject([ProductsService], (service: ProductsService) => {
    expect(service).toBeTruthy();
  }));

  /*it('should initial empty array be returned', (done) => {
    inject([ProductsService], (service: ProductsService) => {
      service.getProducts().pipe(first()).subscribe(products => {
        expect(products.length).toBe(0);
        done();
      });
    })();
  });*/

  it('method getProducts should return an array of Products', (done) => {
    inject([ProductsService], (service: ProductsService) => {
      let currentProductsNumber = -1;
      service.getProducts().pipe(skip(1)).pipe(first()).subscribe(products => {
        currentProductsNumber = products.length;
        expect(currentProductsNumber).toBeGreaterThanOrEqual(0);
        done();
      });
    })();
  });

  it('number of returned products increments after adding new product', (done) => {
    inject([ProductsService], (service: ProductsService) => {
      service.getProducts().pipe(skip(1)).pipe(first()).pipe(map(products => products.length))
        .subscribe(initialProductsNumber => {
          service.addNewProduct('name', 2, 'description', 20, 10, 'imageUrl');
          service.getProducts().pipe(first()).pipe(map(products => products.length))
            .subscribe(productsNumberAfterAddition => {
              expect(initialProductsNumber).toBe(productsNumberAfterAddition - 1);
              done();
            });
        });
    })();
  });

  /*it('number of returned products decremented after removing product', (done) => {
    inject([ProductsService], (service: ProductsService) => {
      service.getProducts().pipe(skip(1)).pipe(first())
        .subscribe(currentProducts => {
          const initialProductsNumber = currentProducts.length;
          if (!initialProductsNumber) {
            expect(true).toBeTruthy();
            done();
          } else {
            service.removeProduct(currentProducts[0].id);
            service.getProducts().pipe(first()).pipe(map(products => products.length))
              .subscribe(productsNumberAfterDeletion => {
                expect(initialProductsNumber).toBe(productsNumberAfterDeletion + 1);
                done();
              });
          }
        });
    })();
  });*/

  it('should quantity of certain product present in cart increase after moving item to cart', (done) => {
    inject([ProductsService], (productService: ProductsService) => {
      productService.getProducts().pipe(skip(1)).pipe(first())
        .subscribe(currentProducts => {
          const productsToMove = currentProducts.filter(product => product.quantity > 1);
          if (!productsToMove.length) {
            console.log('there is no product which has more than one instance');
            expect(true).toBeTruthy();
            done();
          } else {
            productService.putProductToCart(productsToMove[0].id); // we are sure that at least one product is in cart
            productService.getProductsInCart().pipe(skip(1)).pipe(first()).subscribe(productsInCart => {
              const movedProductInitialQuantity =
                productsInCart.filter(product => product.id === productsToMove[0].id)[0].quantity;
              productService.putProductToCart(productsToMove[0].id);
              productService.getProductsInCart().pipe(first()).subscribe(updatedProductsInCart => {
                expect(movedProductInitialQuantity).toBe
                (updatedProductsInCart.filter(product => product.id === productsToMove[0].id)[0].quantity - 1);
                done();
              });
            });
          }
        });
    })();
  });

  it('should quantity of certain product absent in cart be set to one after moving item to cart', (done) => {
    inject([ProductsService], (productService: ProductsService) => {
      let initialProducts: Product[] = [];
      let initialProductsInCart: Product[] = [];
      productService.getProducts().pipe(skip(1)).pipe(first()).toPromise()
        .then(products => {
          initialProducts = products;
        })
        .then(_ => {
          productService.getProductsInCart().pipe(skip(1)).pipe(first()).toPromise()
            .then(productsInCart => {
              initialProductsInCart = productsInCart;
              const productsAbsentInCart = initialProducts.filter(product => {
                return initialProductsInCart.filter(productInCart => productInCart.id === product.id).length === 0;
              });

              if (!productsAbsentInCart.length || !productsAbsentInCart[0].quantity) {
                expect(true).toBeTruthy();
                done();
              } else {
                productService.putProductToCart(productsAbsentInCart[0].id);
                productService.getProductsInCart().pipe(first()).toPromise()
                  .then(updatedProductsInCart => {
                    const newProductInCart =
                      updatedProductsInCart.filter(
                        updatedProductInCart => updatedProductInCart.id === productsAbsentInCart[0].id);
                    expect(newProductInCart[0].quantity).toBe(1);
                    done();
                  });
              }
            });
        });
    })();
  });

  it('should remove product from cart back to shelf', (done) => {
    inject([ProductsService], (productService: ProductsService) => {
      let initialProducts: Product[] = [];
      let productToBeBackFromCart: Product;
      let initialProductsInCart: Product[] = [];
      productService.getProducts().pipe(skip(1)).pipe(first()).toPromise()
        .then(products => {
          initialProducts = products;
        })
        .then(_ => {
          productService.getProductsInCart().pipe(skip(1)).pipe(first()).toPromise()
            .then(productsInCart => {
              initialProductsInCart = productsInCart;
              productToBeBackFromCart = initialProductsInCart.filter(product => product.quantity)[0];

              if (!initialProductsInCart.length) {
                console.log('there was no product in cart');
                expect(true).toBeTruthy();
                done();
              } else {
                const productOnShelfQuantityBeforeModification =
                  initialProducts.filter(product => product.id === productToBeBackFromCart.id)[0].quantity;
                productService.removeProductFromCart(productToBeBackFromCart.id);
                productService.getProducts().pipe(first()).toPromise()
                  .then(products => {
                    const modificatedProduct = products.filter(product => product.id === productToBeBackFromCart.id)[0];
                    console.log('before ', productOnShelfQuantityBeforeModification, 'after ', modificatedProduct.quantity);
                    expect(modificatedProduct.quantity).toBe(productOnShelfQuantityBeforeModification + 1);
                    done();
                  });
              }
            });
        });
    })();
  });
  /*it('number of returned products incremented after adding new product', (done) => {
    inject([ProductsService], (service: ProductsService) => {

    })();
  });*/
});
