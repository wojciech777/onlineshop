import { TestBed } from '@angular/core/testing';

import { FirebaseDatabaseService } from './firebase-database.service';
import {environment} from '../../../../environments/environment';
import {FormsModule} from '@angular/forms';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';

describe('FirebaseDatabaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireAuthModule,
      AngularFireDatabaseModule
    ]
  }));

  it('should be created', () => {
    const service: FirebaseDatabaseService = TestBed.get(FirebaseDatabaseService);
    expect(service).toBeTruthy();
  });
});
