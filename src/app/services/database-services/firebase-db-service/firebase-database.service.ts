import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import {first} from 'rxjs/operators';
import {Product} from '../../../product';
import {Observable} from 'rxjs';
import Reference = firebase.database.Reference;
import {Order} from '../../../order';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDatabaseService {
  private productsDbRef: AngularFireList<any>;
  private productsInCartDbRef: AngularFireList<any>;
  private ordersRef: AngularFireList<any>;

  constructor(private db: AngularFireDatabase) {
    this.productsDbRef = this.db.list('/products');
    this.productsInCartDbRef = this.db.list('/productsInCart');
    this.ordersRef = this.db.list('/orders');
  }

  getProducts() {
    return this.productsDbRef.snapshotChanges();
  }

  getProductsInCart() {
    return this.productsInCartDbRef.snapshotChanges();
  }

  getOrders() {
    return this.ordersRef.snapshotChanges();
  }

  putProductToCart(productId) {
    let currentProduct;
    let currentProductInCart;
    this.getProduct(productId).then(result => {
      currentProduct = result.val();
      console.log(currentProduct);
    })
      .then(_ => this.getProductInCart(productId).then(result2 => {
        currentProductInCart = result2.val();
        console.log('current product in cart is: ', currentProductInCart);
      })).then(_ => {
      if (currentProduct.quantity) {
        currentProduct.quantity--;
        this.updateProduct(productId, currentProduct);
        if (currentProductInCart) {
          currentProductInCart.quantity++;
          this.updateProductInCart(productId, currentProductInCart);
        } else {
          currentProduct.quantity = 1;
          this.updateProductInCart(productId, currentProduct);
        }
      }
    });
  }

  removeProductFromCart(productId: number) {
    this.decreaseProductInCartQuantity(productId);
    this.increaseProductQuantity(productId);
  }

  removeAllProductOccurrencesFromCart(productId: number) {
    this.getProductInCart(productId).then(productInCart => {
      const quantityIncreased = productInCart.val().quantity;
      this.updateProductInCart(productId, {quantity: 0});
      this.increaseProductQuantity(productId, quantityIncreased);
    });
  }

  private increaseProductQuantity(productId: number, quantityIncreased = 1) {
    this.getProduct(productId).then(product => {
      const currentProduct = product.val();
      currentProduct.quantity += quantityIncreased;
      this.updateProduct(productId, currentProduct);
    });
  }

  private decreaseProductInCartQuantity(productId: number) {
    this.increaseProductInCartQuantity(productId, -1);
  }

  private increaseProductInCartQuantity(productId: number, quantityIncreased = 1) {
    this.getProductInCart(productId).then(product => {
      const currentProduct = product.val();
      currentProduct.quantity += quantityIncreased;
      this.updateProductInCart(productId, currentProduct);
    });
  }

  private getProduct(productId) {
    return this.db.database.ref('/products/' + productId).once('value');
  }

  private getProductInCart(productId) {
    return this.db.database.ref('/productsInCart/' + productId).once('value');
  }

  updateProduct(productId: number, productNewProperties: Object) {
    this.productsDbRef.update(productId + '', productNewProperties);
  }

  updateProductInCart(productId: number, productNewProperties: Object) {
    this.productsInCartDbRef.update(productId + '', productNewProperties);
    // this.db.object(`/productsInCart/${newProduct.id}`).set(newProduct);
  }

  updateOrder(order: Order) {
    this.ordersRef.update(order.id + '', order);
  }

  removeProduct(productId: number) {
    this.productsDbRef.remove(productId + '');
  }
}
