import { Injectable } from '@angular/core';
import {Product} from '../../product';

@Injectable({
  providedIn: 'root'
})
export class AbstractDatabaseService {
  public emptyProduct: Product = new Product('', 2, '', undefined, undefined, new Date(), '');
  constructor() { }
}
