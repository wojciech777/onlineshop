import { TestBed } from '@angular/core/testing';

import { AbstractDatabaseService } from './abstract-database.service';

describe('AbstractDatabaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbstractDatabaseService = TestBed.get(AbstractDatabaseService);
    expect(service).toBeTruthy();
  });
});
