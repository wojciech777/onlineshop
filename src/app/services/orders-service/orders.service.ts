import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Order} from '../../order';
import {OrderStatus} from '../../order-status.enum';
import {Product} from '../../product';
import {ProductOrdered} from '../../product-ordered';
import {FirebaseDatabaseService} from '../database-services/firebase-db-service/firebase-database.service';
import {first} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private orders: BehaviorSubject<Order[]> = new BehaviorSubject<Order[]>([]);

  constructor(private firebaseDbManager: FirebaseDatabaseService) {
    console.log('inicjalizacja serwisu zamowien');
    firebaseDbManager.getOrders().subscribe(orders => {
      this.orders.next(this.OrdersSnapshotToArray(orders));
    });
  }

  public getOrders(): Observable<Order[]> {
    return this.orders.asObservable();
  }

  public addOrder(products: Product[], address, addressee) {
    const fakeClientId = 1;
    const newOrderId = this.findCurrentMaxId() + 1;
    const productsOrdered: ProductOrdered[] = [];
    products.forEach(product => productsOrdered.push(new ProductOrdered(product)));
    const createdOrder = new Order(newOrderId, fakeClientId, address, addressee, productsOrdered);
    this.firebaseDbManager.updateOrder(createdOrder);
    this.orders.getValue().push(createdOrder);
    this.orders.next(this.orders.getValue());
  }

  public markOrderedProductAsCompleted(orderId: number, productCompletedId: number) {
    const updatedOrder = this.getOrder(orderId);
    updatedOrder.products.forEach(product => {
      if (product.id === productCompletedId) {
        product.packed = true;
      }
    });
    if (this.existAnyUncompletedProduct(orderId)) {
      this.markOrderAsPartiallyComplete(orderId);
    } else {
      this.markOrderAsRealized(orderId);
    }

    this.firebaseDbManager.updateOrder(this.getOrder(orderId));
    this.orders.next(this.orders.getValue());
  }

  public markOrderAsRealized(orderId: number) {
    this.changeOrderStatus(orderId, OrderStatus.Realized);
    this.setOrderRealizationDate(orderId);
    this.markOrderAllProductsAsCompleted(orderId);
    this.firebaseDbManager.updateOrder(this.getOrder(orderId));
    this.orders.next(this.orders.getValue());
  }

  private markOrderAsPartiallyComplete(orderId: number) {
    this.changeOrderStatus(orderId, OrderStatus.PartiallyComplete);
    this.orders.next(this.orders.getValue());
  }

  private changeOrderStatus(orderId: number, newStatus: OrderStatus) {
    this.orders.getValue().forEach(order => {
      if (order.id === orderId) {
        order.status = newStatus;
      }
    });
  }

  private setOrderRealizationDate(orderId: number) {
    this.orders.getValue().forEach(order => {
      if (order.id === orderId) {
        order.realizationDate = new Date();
      }
    });
  }

  private findCurrentMaxId(): number {
    return this.orders.getValue().reduce(function (previousValue, currentValue) {
      return (currentValue.id > previousValue) ? currentValue.id : previousValue;
    }, -1);
  }

  private markOrderAllProductsAsCompleted(orderId: number) {
    const updatedOrder: Order = this.getOrder(orderId);
    updatedOrder.products.forEach(orderedProduct => orderedProduct.packed = true);
  }

  private getOrder(orderId: number): Order {
    return this.orders.getValue().filter(order => order.id === orderId)[0];
  }

  private OrdersSnapshotToArray(snapshot) {
    const returnArr: Order[] = [];

    snapshot.forEach(function (childSnapshot) {
      const item = childSnapshot.payload.val();
      const orderedProducts: ProductOrdered[] = [];
      item.products.forEach(product => {
        const orderedProduct = new ProductOrdered(
          new Product(product.name, product.type, product.description, product.quantity,
            product.price, product.shortageTime, product.imageUrl));
        orderedProduct.packed = product.packed;
        orderedProduct.id = product.id;
        orderedProducts.push(orderedProduct);
      });
      const order: Order = new Order(Number(childSnapshot.key), 1, item.destinationAddress, item.addressee, orderedProducts);
      order.status = item.status;
      order.createDate = item.createDate;
      order.realizationDate = item.realizationDate;
      order.estimatedRealizationTime = item.estimatedRealizationTime;

      returnArr.push(order);
    });

    return returnArr;
  }

  private existAnyUncompletedProduct(orderId: number) {
    const order: Order = this.getOrder(orderId);
    return !order.products.reduce(function (previousValue, currentValue) {
      return previousValue && currentValue.packed;
    }, true);
  }
}
