import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {$} from 'protractor';
import {AuthServiceService} from './auth-service.service';
import {Subscription} from 'rxjs';
import {AngularFireDatabase} from 'angularfire2/database';
import {Product} from './product';
import {first} from 'rxjs/operators';


@Component({
  selector: 'app-component',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loggedEmail: string;
  adminLogged: boolean;
  authServiceSubscription: Subscription;
  products: Product[] = [];


  constructor(private authService: AuthServiceService) {
    this.authServiceSubscription = this.authService.authState$.subscribe(state => {
      this.adminLogged = false;
      if (state !== null) {
        this.loggedEmail = state.email;
        if (this.loggedEmail.indexOf('admin') !== -1) {
          this.adminLogged = true;
        }
      } else {
        this.loggedEmail = '';
      }
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.authServiceSubscription.unsubscribe();
  }

  logOutCurrentUser() {
    this.authService.logout().then(function () {
      console.log('Logged out from FireBase!');
    }, function (error) {
      console.log(error.code);
      console.log(error.message);
    });
  }

  /*public gotoTop() {
    $('html, body').animate({scrollTop: 0}, 'slow');
  }*/
}
