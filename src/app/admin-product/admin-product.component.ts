import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../product';
import {ProductsService} from '../services/products-service/products.service';

@Component({
  selector: 'app-admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.css']
})
export class AdminProductComponent implements OnInit {
  @Input() product: Product;
  productCopy: Product;

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.productCopy = new Product(this.product.name, this.product.type,
      this.product.description, this.product.quantity, this.product.price, this.product.shortageTime, this.product.imageUrl);
    this.productCopy.id = this.product.id;
  }

  public removeProduct() {
    this.productsService.removeProduct(this.product.id);
  }
}
