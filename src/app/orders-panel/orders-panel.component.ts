import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrdersService} from '../services/orders-service/orders.service';
import {Subscription} from 'rxjs';
import {Order} from '../order';
import {OrderStatus} from '../order-status.enum';

@Component({
  selector: 'app-orders-panel',
  templateUrl: './orders-panel.component.html',
  styleUrls: ['./orders-panel.component.css']
})
export class OrdersPanelComponent implements OnInit, OnDestroy {
  private ordersServiceSubscription: Subscription;
  public orders: Order[];
  realizedOrdersVisible = false;

  constructor(private ordersService: OrdersService) {
  }

  ngOnInit() {
    this.ordersServiceSubscription = this.ordersService.getOrders().subscribe(orders => {
      console.log('przyszly nowe zamowienia do orders-panel', orders);
      this.orders = orders;
    });
  }

  ngOnDestroy(): void {
    this.ordersServiceSubscription.unsubscribe();
  }

  public markOrderAsRealized(orderId: number) {
    this.ordersService.markOrderAsRealized(orderId);
  }

  public markOrderedProductAsCompleted(orderId: number, productId: number) {
    this.ordersService.markOrderedProductAsCompleted(orderId, productId);
  }

  getFilteredOrders() {
    if (this.realizedOrdersVisible) {
      return this.orders;
    }
    return this.orders.filter(order => order.status !== OrderStatus.Realized);
  }
}
