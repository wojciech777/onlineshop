import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthServiceService} from '../auth-service.service';
import {Product} from '../product';
import {ProductsService} from '../services/products-service/products.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit, OnDestroy {
  public emptyProduct: Product = new Product('', 2, '', undefined, undefined, new Date(), '');
  public products: Product[];
  public ordersViewSelected = true;
  public productsViewSelected = false;
  private productServiceSubscription: Subscription;
  private authServiceSubscription: Subscription;
  public productsFilter: string;
  public loggedEmail: string;
  public assistantLogged: boolean;

  constructor(private productsService: ProductsService, private authService: AuthServiceService) {
    this.authServiceSubscription = this.authService.authState$.subscribe(state => {
      this.loggedEmail = state.email;
      if (this.loggedEmail.indexOf('assist') !== -1) {
        this.assistantLogged = true;
      }
    });
  }

  ngOnInit() {
    this.productServiceSubscription = this.productsService.getProducts().subscribe(products => {
      this.products = products;
    });
  }

  ngOnDestroy() {
    this.productServiceSubscription.unsubscribe();
    this.authServiceSubscription.unsubscribe();
  }

  public setOrdersView() {
    this.ordersViewSelected = true;
    this.productsViewSelected = false;
  }

  public setProductsView() {
    this.ordersViewSelected = false;
    this.productsViewSelected = true;
  }

  public filterProducts() {
    if (!this.productsFilter) {
      return this.products;
    }
    return this.products.filter(product => product.name.toLowerCase().indexOf(this.productsFilter.toLowerCase()) !== -1);
  }
}
