// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.

const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

exports.predictShortageOfProducts = functions.database.ref('/orders/{orderID}/products')
  .onCreate((snapshot, context) => {
    const orderedProducts = snapshot.val();

    console.log('Ordered products are: ', context.params.orderID, orderedProducts);

    return admin.database().ref(`/orders/`).once('value')
      .then( function(ordersSnapshot) {
        orderedProducts.forEach(product => {
          return admin.database().ref(`products/${product.id}/quantity`).once('value')
            .then(function (productQuantitySnapshot) {
              return admin.database().ref(`products/${product.id}`).child('shortageTime').set(evaluateProductShortageDate(ordersSnapshot.val(), product, productQuantitySnapshot.val()) + '');
            });
        });

        snapshot.ref.parent.child("estimatedRealizationTime").set(evaluateRealizationDate(ordersSnapshot.val(),orderedProducts.length) + "");

        return null;
      });
  });


function evaluateRealizationDate(orders, orderedProductsNumber) {
  // console.log("orders and orderedProductsNumber: ", orders, orderedProductsNumber);
  let sampleOrders = orders.filter(order => order.products.length === orderedProductsNumber && order.status === 3);
  // console.log("orders of desired size number is: ", sampleOrders.length);
  if(sampleOrders.length === 0){
    console.log("unable to estimate - no order with exactly that size");
    return "";
  }

  let allRealizationTimes = sampleOrders.map(order => new Date(order.realizationDate) - new Date(order.createDate));
  let averageRealizationTime =
    allRealizationTimes.reduce((a, b) => a + b) / sampleOrders.length;

  // console.log("all Realization Times number: ", allRealizationTimes);
  console.log("averageRealizationTime is: ", averageRealizationTime);
  let estimatedCompletionTime = new Date(Date.now() + averageRealizationTime);
  console.log("Estimated completion time is: ", estimatedCompletionTime);
  return estimatedCompletionTime;
}

function evaluateProductShortageDate(allOrders, productOrdered, productQuantity) {
  // console.log("dla: ", productOrdered, allOrders);

  const ordersForProduct = allOrders.filter(order => {
    return order.products.some(p => p.id === productOrdered.id);
  });

  if (ordersForProduct.length <= 1) {
    console.log('unable to estimate shortage time for product - too little historical data');
    return '';
  }

  // console.log("orders for product: ", ordersForProduct);

  const sumOfOrdersQuantity = ordersForProduct.map(order => order.products.filter(p => p.id === productOrdered.id)[0].quantity)
    .reduce(function(previousValue, currentValue, index, array) {
              return previousValue + currentValue;
            });

  const firstDate = ordersForProduct.map(order => (new Date(order.createDate)).getTime()).sort((a, b) => {return a - b})[0];
  const lastDate = ordersForProduct.map(order => (new Date(order.createDate)).getTime()).sort((a,b) => {return a - b})[ordersForProduct.length - 1];

  // console.log()
  // console.log("differences of dates", lastDate - firstDate);
  // console.log("sumOfOrders: ", sumOfOrdersQuantity);
  // console.log("diff / sum", (lastDate - firstDate) / sumOfOrdersQuantity);
  // console.log("diff / sum * productQuantity", (lastDate - firstDate) / sumOfOrdersQuantity * productQuantity);
  // console.log("Date.now() + previous", Date.now() + (lastDate - firstDate) / sumOfOrdersQuantity * productQuantity);
  // console.log("new Date(previous)", new Date(Date.now() + (lastDate - firstDate) / sumOfOrdersQuantity * productQuantity));
  // console.log("lastDate firstDate productQuantity sumOfOrdersQuantity ", lastDate, firstDate, productQuantity, sumOfOrdersQuantity);


  let newShortageTime = new Date(Date.now() + (lastDate - firstDate) / sumOfOrdersQuantity * productQuantity);
  console.log(`new shortage time for product of id: ${productOrdered.id} is ${newShortageTime}`);
  return newShortageTime;
}
